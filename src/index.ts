import app from './App'
import { connect } from './database/dbConnection'

//connect to database when start the server..
connect();
app.listen(8080, 'localhost', function (err) {
    if (err) {
        console.log('error starting server', + err)
        return;
    }

    console.log('server started successfully on port 8080')
});